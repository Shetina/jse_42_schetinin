package ru.t1.schetinin.tm.exception.field;

public class ExistsEmailException extends AbstractFieldException {

    public ExistsEmailException() {
        super("Error! Email already exists...");
    }

}