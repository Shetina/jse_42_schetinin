package ru.t1.schetinin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.schetinin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.schetinin.tm.api.service.IAuthService;
import ru.t1.schetinin.tm.api.service.IServiceLocator;
import ru.t1.schetinin.tm.api.service.IUserService;
import ru.t1.schetinin.tm.dto.request.UserLoginRequest;
import ru.t1.schetinin.tm.dto.request.UserLogoutRequest;
import ru.t1.schetinin.tm.dto.request.UserViewProfileRequest;
import ru.t1.schetinin.tm.dto.response.UserLoginResponse;
import ru.t1.schetinin.tm.dto.response.UserLogoutResponse;
import ru.t1.schetinin.tm.dto.response.UserViewProfileResponse;
import ru.t1.schetinin.tm.dto.model.SessionDTO;
import ru.t1.schetinin.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.schetinin.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IAuthService getAuthService() {
        return this.getServiceLocator().getAuthService();
    }

    @NotNull
    private IUserService getUserService() {
        return this.getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        try {
            @NotNull final IAuthService authService = getServiceLocator().getAuthService();
            @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
            return new UserLoginResponse(token);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        final SessionDTO session = check(request);
        try {
            getAuthService().logout(session);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserViewProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserViewProfileRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDTO user;
        try {
            user = getServiceLocator().getUserService().findOneById(userId);
        } catch (@NotNull final Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        return new UserViewProfileResponse(user);
    }

}